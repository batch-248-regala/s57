let collection = [];

// Write the queue functions below.

function print() {
    return collection;
}

function enqueue(element) {
  let collectionLength = collection.length;
  collection[collectionLength] = element;
  return collection;
}


function dequeue() {
  let removedElement = collection[0];
  for (let i = 0; i < collection.length - 1; i++) {
    collection[i] = collection[i + 1];
  }
  collection.length = collection.length - 1;
  return removedElement;
}

function front() {
    return collection[0];  
}

function size() {
    return collection.length;
}

function isEmpty() {
   return collection.length === 0;
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
